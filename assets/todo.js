$('document').ready(function () {


    if (taskManager.getData() == null) {
        var testData = {"lists": [
            {"title": "Buy", "date": "08.02.2017", "addTime": 1485627611383, "id": "9aa8bb88-98b9-49ab-b9b8-a8998bba9abb"},
            {"title": "To-do list", "date": "31.01.2017", "addTime": 1485627765297, "id": "9888899a-8b9b-4a8a-989b-aa88aaa8a9aa"}
        ], "tasks": [
            {"title": "Buy shawarma", "date": "29.01.2017", "addTime": 1485627623265, "listID": "9aa8bb88-98b9-49ab-b9b8-a8998bba9abb", "id": "88b8a8ba-baa8-499b-8b88-aa8a89a98899", "type": "deleted"},
            {"title": "Buy potatoes", "date": "31.01.2017", "addTime": 1485627632932, "listID": "9aa8bb88-98b9-49ab-b9b8-a8998bba9abb", "id": "aab89ab9-bb8a-4baa-9999-a8ab98a9ab89", "type": "deleted"},
            {"title": "Make TODO", "date": "28.01.2017", "addTime": 1485627694672, "id": "bbb9b99b-8ab8-4ab9-b8b9-bab89a99a9a9", "type": "completed"},
            {"title": "Do something", "date": "31.01.2017", "addTime": 1485627784306, "listID": "9888899a-8b9b-4a8a-989b-aa88aaa8a9aa", "id": "899998b9-9998-4ab9-a8a8-a9a8889abb99"},
            {"title": "Do something else", "date": "31.01.2017", "addTime": 1485627797578, "listID": "9888899a-8b9b-4a8a-989b-aa88aaa8a9aa", "id": "8aab898b-99b9-4888-88a9-b9b9a8b8b9b8"},
            {"title": "Make something else", "date": "31.01.2017", "addTime": 1485627819179, "listID": "9888899a-8b9b-4a8a-989b-aa88aaa8a9aa", "id": "9b889b99-8a89-4bb8-abb8-aab8ab8ab89a"}
        ]};

        localStorage.setItem("todo", JSON.stringify(testData));
    }

    taskManager.parseByDate();
    $('.tab').on('click', function () {
        var id = $(this).attr('id');
        $('.tab').removeClass('active');
        $(this).addClass('active');

        if (id == 'byDate')taskManager.parseByDate();
        if (id == 'completed')taskManager.parseByCompleted();
        if (id == 'deleted')taskManager.parseByDeleted();
        if (id == 'lists')taskManager.parseByLists();
    });

    $('#addTask').on('click', function () {
        taskManager.addTask();
    });

    $('#addTaskList').on('click', function () {
        taskManager.addTaskList();
    });
});

