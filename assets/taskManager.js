TaskManager = function () {

};


var taskManager = TaskManager.prototype = {



    /**
     * получить из ЛС
     */
    getData: function () {
        var data = localStorage.getItem("todo");

        return JSON.parse(data);
    },


    parseByDate: function () {
        var data = this.getData();
        var self = this;

        if (data != null) {

            $('.content').empty();

            var tasks = self.sorter(data.tasks);

            _.each(tasks, function (t, i) {
                if (!t.type) {

                    var listName = t.listID ? "From List: " + self.getListById(t.listID).title : 'Single Task';

                    $('.content').append(
                        '<div class="taskByDate" taskId="' + t.id + '"> ' +

                            '<span class="title">TASK: ' + t.title + '</span><br>' +
                            '<span class="date">' + t.date + '</span><br>' +
                            '<span>' + listName + '</span>' +
                            '<span class="updateType" setTaskTo="deleted"><img src="img/delete.png"></span>' +
                            '<span class="updateType" setTaskTo="completed"><img src="img/check.png"></span>' +
                            '<span class="edit"><img src="img/edit.png"></span>' +
                            '</div>'
                    );
                }
            });

            $('.edit').on('click', function () {
                var id = $(this).parent().attr('taskId');

                self.editById(id);
            });


            $('.updateType').on('click', function () {
                var setTaskTo = $(this).attr('setTaskTo');
                var taskID = $(this).parent().attr('taskId');

                var found = _.find(tasks, {id: taskID});

                if (found) {
                    found.type = setTaskTo;
                    self.updateData(found);
                    self.updateView();
                }
            });
        }
    },


    getListById:function (id){
        var data = this.getData();

        return _.find(data.lists, {id:id});
    },


    parseByCompleted: function () {
        var data = this.getData();
        var self = this;

        if (data != null) {

            $('.content').empty();


            var tasks = self.sorter(data.tasks);

            _.each(tasks, function (t, i) {
                if (t.type == 'completed') {
                    var listName = t.listID ? "From List: " + self.getListById(t.listID).title : 'Single Task';
                    $('.content').append(
                        '<div class="taskByCompleted" taskId="' + t.id + '"> ' +

                            '<span class="title">TASK: ' + t.title + '</span><br>' +
                            '<span class="date">' + t.date + '</span><br>' +
                            '<span>' + listName + '</span>' +
                            '<span class="updateType" setTaskTo="deleted"><img src="img/delete.png"></span>' +
                            '<span class="updateType" setTaskTo="restored"><img src="img/restore.png"></span>' +
                            '<span class="edit"><img src="img/edit.png"></span>' +
                            '</div>'
                    );
                }
            });

            $('.edit').on('click', function () {
                var id = $(this).parent().attr('taskId');

                self.editById(id);
            });


            $('.updateType').on('click', function () {
                var setTaskTo = $(this).attr('setTaskTo');
                var taskID = $(this).parent().attr('taskId');

                var found = _.find(tasks, {id: taskID});

                if (found) {
                    setTaskTo == 'restored' ? delete found.type : found.type = setTaskTo;
                    self.updateData(found);
                    self.updateView();
                }
            });
        }
    },

    sorter:function (data){
        return data.sort(function (a, b) {
            var aa = a.date.split('.').reverse().join(),
                bb = b.date.split('.').reverse().join();
            return aa < bb ? -1 : (aa > bb ? 1 : 0);
        });
    },

    parseByDeleted: function () {
        var data = this.getData();
        var self = this;

        if (data != null) {

            $('.content').empty();

            var tasks = self.sorter(data.tasks);

            _.each(tasks, function (t, i) {
                if (t.type == 'deleted') {
                    var listName = t.listID ? "From List: " + self.getListById(t.listID).title : 'Single Task';
                    $('.content').append(
                        '<div class="taskByCompleted" taskId="' + t.id + '"> ' +

                            '<span class="title">TASK: ' + t.title + '</span><br>' +
                            '<span class="date">' + t.date + '</span><br>' +
                            '<span>' + listName + '</span>' +
                            '<span class="updateType" setTaskTo="cleared"><img src="img/delete.png"></span>' +
                            '<span class="updateType" setTaskTo="restored"><img src="img/restore.png"></span>' +
                            '<span class="edit"><img src="img/edit.png"></span>' +
                            '</div>'
                    );
                }
            });

            $('.edit').on('click', function () {
                var id = $(this).parent().attr('taskId');

                self.editById(id);
            });

            $('.updateType').on('click', function () {
                var setTaskTo = $(this).attr('setTaskTo');
                var taskID = $(this).parent().attr('taskId');

                var found = _.find(tasks, {id: taskID});

                if (found) {
                    setTaskTo == 'restored' ? delete found.type : found.type = setTaskTo;
                    self.updateData(found);
                    self.updateView();
                }
            });
        }
    },

    parseByLists: function () {
        var data = this.getData();
        var self = this;

        if (data != null) {

            $('.content').empty();

            var lists = self.sorter(data.lists);

            _.each(lists, function (l, i) {
                $('.content').append(
                    '<div class="taskByCompleted" taskId="' + l.id + '"> ' +

                        '<span class="title">LIST: ' + l.title + '</span><br>' +
                        '<span class="date">' + l.date + '</span><br>' +
                        '<span class="updateType deleted"><img src="img/delete.png"></span>' +
                        '<span class="updateType addTaskToList"><img src="img/add.png"></span>' +
                        '<span class="edit"><img src="img/edit.png"></span>' +
                        '</div>'
                );

                var tasks = self.sorter(data.tasks);

                _.each(tasks, function (t, i) {
                    if (t.listID && t.listID == l.id) {

                        var type = t.type ? t.type : 'In Progress';
                        $("[taskId=" + l.id + "]").append(
                            '<hr>' +
                                '<div class="taskByDate" taskId="' + t.id + '"> ' +

                                '<span class="title">TASK: ' + t.title + '</span><br>' +
                                '<span class="date">' + t.date + '</span><br>' +
                                '<span class="status">STATUS: ' + type + '</span><br>' +
                                '<span class="edit inList"><img src="img/edit.png"></span>' +
                                '</div>'

                        );
                    }
                });
            });


            $('.edit').on('click', function () {
                var id = $(this).parent().attr('taskId');

                self.editById(id);
            });


            $('.addTaskToList').on('click', function () {
                var listID = $(this).parent().attr('taskId');

                if (!$('.addTaskToListForm').length) {
                    $("[taskId=" + listID + "]").append(
                        '<div>' +
                            '<form class="addTaskToListForm">' +
                            '<input id="addTaskToListTitle" type="text" placeholder="Title"/>' +
                            '<input id="addTaskToListDate" type="text" placeholder="Due Date" readonly="true"/>' +
                            '<span class="btnPrimary" id="addTaskToList">Add Task</span>' +
                            '<span class="btnPrimary" id="Cancel">Cancel</span>' +
                            '</form>' +
                            '</div>'

                    );

                    $("#addTaskToListDate").datepicker();
                    $("#addTaskToListDate").datepicker("option", "dateFormat", "dd.mm.yy");

                    $('#addTaskToList').on('click', function () {
                        self.setTaskToList(listID);
                    });

                    $('#Cancel').on('click', function () {
                        $('.addTaskToListForm').parent().remove();
                    });
                }

            });


            $('.deleted').on('click', function () {
                var setListTo = 'deleted';
                var listID = $(this).parent().attr('taskId');

                var found = _.find(lists, {id: listID});

                if (found) {
                    found.type = setListTo;
                    self.updateData(found, true);
                    self.updateView();
                }
            });
        }
    },

    
    editById: function (id) {
        var title = $("[taskId=" + id + "] > .title").text();
        var date = $("[taskId=" + id + "] > .date").text();
        var data = this.getData();
        var self = this;

        if (!$('.addTaskToListForm').length) {
            $("[taskId=" + id + "]").append(
                '<div>' +
                    '<form class="addTaskToListForm">' +
                    '<input id="addTaskToListTitle" type="text" value="' + title.split(': ')[1] + '"/>' +
                    '<input id="addTaskToListDate" type="text" readonly="true"/>' +
                    '<span class="btnPrimary" id="addTaskToList">Submit</span>' +
                    '<span class="btnPrimary" id="Cancel">Cancel</span>' +
                    '</form>' +
                '</div>'

            );

            $("#addTaskToListDate").datepicker();
            $("#addTaskToListDate").datepicker("option", "dateFormat", "dd.mm.yy");
            $('#addTaskToListDate').val(date);

            $('#addTaskToList').on('click', function () {
                var lists = data.lists;
                var tasks = data.tasks;

                var found = _.find(lists, {id:id});
                if(!found) found = _.find(tasks, {id:id});

                found.title = $("#addTaskToListTitle").val();
                found.date = $("#addTaskToListDate").val();
                
                localStorage.setItem("todo", JSON.stringify(data));
                $('.addTaskToListForm').parent().remove();
                self.updateView();
            });

            $('#Cancel').on('click', function () {
                $('.addTaskToListForm').parent().remove();
            });
        }
    },
    

    clearTasks: function (id, data) {
        var tasks = data.tasks;
        for (var i = (tasks.length - 1); i >= 0; i--) {
            if (tasks[i].listID && tasks[i].listID == id)
                tasks.splice(i, 1);
        }
    },


    setTaskToList: function (listID) {
        var valid = this.validateTask();

        if (valid) {
            var data = {
                title: $('#addTaskToListTitle').val(),
                date: $('#addTaskToListDate').val(),
                addTime: new Date().getTime(),
                listID: listID,
                id: this.getId()
            };

            this.setData(true, data);
            $('.addTaskToListForm').parent().remove();

            this.parseByLists();
        }
    },


    updateData: function (newDate, list) {
        var data = this.getData();
        var self = this;

        if (list) {
            _.each(data.lists, function (l, i) {
                if (l.id == newDate.id) {
                    if (newDate.type == 'deleted') {
                        data.lists.splice(i, 1);

                        self.clearTasks(newDate.id, data);

                        return false;
                    }
                    else data.lists[i] = newDate;
                }
            });

        } else {
            _.each(data.tasks, function (t, i) {
                if (t.id == newDate.id) {
                    if (newDate.type == 'cleared') {
                        data.tasks.splice(i, 1);
                        return false;
                    }
                    else data.tasks[i] = newDate;
                }
            });
        }


        localStorage.setItem("todo", JSON.stringify(data));
    },


    /**
     * сохрвнить в ЛС
     * task - bool, если true, то сохраняемтаск, если нет, то новый список
     * newData - obj, кагбэ ясно, что новые данные
     */
    setData: function (task, newData) {
        var data = this.getData();
        if (data == null) {
            data = {
                lists: [],
                tasks: []
            };
        }

        if (task) data.tasks.push(newData);
        else data.lists.push(newData);


        localStorage.setItem("todo", JSON.stringify(data));
        $('#taskTitle, #taskDescription, #datepicker').val('');

    },


    /**
     * создать таск
     */
    addTask: function () {
        var valid = this.validate();

        if (valid) {
            var data = this.parseData();
            this.setData(true, data);
        }

        $('.tab').removeClass('active');
        $('#byDate').addClass('active');

        this.updateView();
    },


    /**
     * создать лист
     */
    addTaskList: function () {
        var valid = this.validate();

        if (valid) {
            var data = this.parseData();
            this.setData(false, data);
        }

        $('.tab').removeClass('active');
        $('#lists').addClass('active');

        this.updateView();
    },


    /**
     * валидация 
     */
    validate: function () {
        var taskTitle = $('#taskTitle').val();
        var date = $('#datepicker').val();


        if (!taskTitle) {
            $('#taskTitle').attr('placeholder', 'Set Title');
            $('#taskTitle').addClass('hint');

            setTimeout("$('#taskTitle').removeClass('hint');$('#taskTitle').attr('placeholder','Title');", 1500)
        }
        if (!date) {
            $('#datepicker').attr('placeholder', 'Pic a Date');
            $('#datepicker').addClass('hint');

            setTimeout("$('#datepicker').removeClass('hint');$('#datepicker').attr('placeholder','Due Date')", 1500)
        }

        return date && taskTitle ? true : false;
    },


    validateTask: function () {
        var taskTitle = $('#addTaskToListTitle').val();
        var date = $('#addTaskToListDate').val();


        if (!taskTitle) {
            $('#addTaskToListTitle').attr('placeholder', 'Set Title');
            $('#addTaskToListTitle').addClass('hint');

            setTimeout("$('#addTaskToListTitle').removeClass('hint');$('#addTaskToListTitle').attr('placeholder','Title');", 1500)
        }
        if (!date) {
            $('#addTaskToListDate').attr('placeholder', 'Pic a Date');
            $('#addTaskToListDate').addClass('hint');

            setTimeout("$('#addTaskToListDate').removeClass('hint');$('#addTaskToListDate').attr('placeholder','Due Date')", 1500)
        }

        return date && taskTitle ? true : false;
    },

    
    getId: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function () {
            var a = 16 * Math.random() | 0, b = 16 * Math.random() | 0,
                c = 'x' === a ? b : 3 & b | 8;
            return c.toString(16);
        });
    },


    parseData: function () {
        return {
            title: $('#taskTitle').val(),
            //description:$('#taskDescription').val(),
            date: $('#datepicker').val(),
            addTime: new Date().getTime(),
            id: this.getId()
        };
    },


    updateView: function () {
        var listType = $('.active').attr('id');

        if (listType == 'byDate') this.parseByDate();
        if (listType == 'completed') this.parseByCompleted();
        if (listType == 'deleted') this.parseByDeleted();
        if (listType == 'lists') this.parseByLists();

    }

};